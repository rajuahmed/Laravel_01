<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/home', 'homeController@home');
Route::get('/about','aboutController@about');
Route::get('/contact','contactController@contact');
Route::get('/gallary','gallaryController@gallary');
Route::get('/service','serviceController@service');
Route::get('/index','indexController@index');
Route::get('/main','mainController@main');
Route::get('/',function(){
  return view('welcome');
});
